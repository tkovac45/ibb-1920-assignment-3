package org.tensorflow.demo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.*;
import android.widget.Toast;

import org.tensorflow.demo.env.ImageUtils;
import org.tensorflow.demo.env.Logger;
import org.tensorflow.demo.tracking.MultiBoxTracker;
import org.tensorflow.lite.demo.R;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

/**
 * Created by TimotejK on 31. 08. 2018.
 */

public class TensorFlowRecognizer  {

    private static final String TAG = TensorFlowRecognizer.class.getSimpleName();

    private static final int TF_OD_API_INPUT_SIZE = 300;
    private static final String TF_OD_API_MODEL_FILE =
            //"file:///android_asset/ssd_mobilenet_v1_android_export.pb";
                        "detect.tflite";
            //"inceptionv3_slim_2016.tflite";

    private static final boolean TF_OD_API_IS_QUANTIZED = true;



    //"file:///android_asset/frozen_inference_graph.pb";
    //private static final String TF_OD_API_LABELS_FILE = "coco_labels_list.txt";
    private static final String TF_OD_API_LABELS_FILE = "labelmap.txt";
            //"imagenet_slim_labels.txt";

    protected static int previewWidth = 0;
    protected static int previewHeight = 0;
    private static Classifier detector;
    private static final Logger LOGGER = new Logger();

    private static Matrix frameToCropTransform;
    private static Matrix cropToFrameTransform;
    private static Integer sensorOrientation;

    private static final boolean MAINTAIN_ASPECT = false;

    // Minimum detection confidence to track a detection.
    private static final float MINIMUM_CONFIDENCE_TF_OD_API = 0.6f;

    private static long timestamp = 0;


    private static MultiBoxTracker tracker;

    private static byte[] luminanceCopy;
    private static Bitmap rgbFrameBitmap = null;
    private static byte[][] yuvBytes = new byte[3][];
    private static int yRowStride;



    protected static byte[] getLuminance() {
        return yuvBytes[0];
    }

    protected static int getLuminanceStride() {
        return yRowStride;
    }

    public static List<Classifier.Recognition> recognizeObjects(Context context, Point point, int rotation, int screenOrientation, OverlayView overlayView, final Bitmap bitmap, Locale locale) {
        return recognizeObjects(context, null, point, rotation, screenOrientation, overlayView, bitmap, locale, MINIMUM_CONFIDENCE_TF_OD_API);
    }


    public static List<Classifier.Recognition> recognizeObjects(Context context, final byte[] bytes, Point point, int rotation, int screenOrientation, OverlayView overlayView, final Bitmap bitmap, Locale locale, float minimumConfidence) {

        int cropSize = TF_OD_API_INPUT_SIZE;

        if (overlayView != null) {
            tracker = new MultiBoxTracker(context);
        }

        try {
            Log.d(TAG, "recognizeObjects: assets = " + Arrays.toString(context.getApplicationContext().getAssets().list("")));

            if (detector == null) {
                String apiLabelsFile = TF_OD_API_LABELS_FILE;
                detector = TFLiteObjectDetectionAPIModel.create(
                        context.getAssets(),
                        TF_OD_API_MODEL_FILE,
                        apiLabelsFile,
                        TF_OD_API_INPUT_SIZE,
                        TF_OD_API_IS_QUANTIZED);
                cropSize = TF_OD_API_INPUT_SIZE;
            }

        } catch (Exception e) {
            Log.e(TAG, "recognizeObjects: exception = " + e.getMessage());
            e.printStackTrace();

            Toast toast = Toast.makeText(context, "Classifier could not be initialized", Toast.LENGTH_SHORT);
            toast.show();

            return null;
        }


        previewWidth = point.x;
        previewHeight = point.y;

        yRowStride = previewWidth;
        yuvBytes[0] = bytes;

        sensorOrientation = rotation - screenOrientation;
        Log.d(TAG, String.format("Camera orientation relative to screen canvas: %d", sensorOrientation));

        LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
        Bitmap croppedBitmap = Bitmap.createBitmap(cropSize, cropSize, Bitmap.Config.ARGB_8888);

        frameToCropTransform =
                ImageUtils.getTransformationMatrix(
                        previewWidth, previewHeight,
                        cropSize, cropSize,
                        sensorOrientation, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        frameToCropTransform.invert(cropToFrameTransform);


        ++timestamp;
        final long currTimestamp = timestamp;
        if (overlayView != null) {
            overlayView.addCallback(
                    new OverlayView.DrawCallback() {
                        @Override
                        public void drawCallback(final Canvas canvas) {
                            Log.d(TAG, "drawCallback: draw called.");
                            tracker.draw(canvas);
                            /*if (isDebug()) {
                                tracker.drawDebug(canvas);
                            }*/
                        }
                    });



            byte[] originalLuminance = getLuminance();
            tracker.onFrame(
                    previewWidth,
                    previewHeight,
                    getLuminanceStride(),
                    sensorOrientation,
                    originalLuminance,
                    timestamp);
            overlayView.postInvalidate();

            // No mutex needed as this method is not reentrant.
            LOGGER.i("Preparing image " + currTimestamp + " for detection in bg thread.");

            //rgbFrameBitmap.setPixels(getRgbBytes(), 0, previewWidth, 0, 0, previewWidth, previewHeight);

            if (luminanceCopy == null) {
                luminanceCopy = new byte[originalLuminance.length];
            }
            System.arraycopy(originalLuminance, 0, luminanceCopy, 0, originalLuminance.length);

        }


        // For examining the actual TF input.

        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(bitmap, frameToCropTransform, null);

        final List<Classifier.Recognition> results = detector.recognizeImage(croppedBitmap);

        final List<Classifier.Recognition> mappedRecognitions = new LinkedList<>();

        for (final Classifier.Recognition result : results) {
            final RectF location = result.getLocation();
            if (location != null && result.getConfidence() >= minimumConfidence) {

                cropToFrameTransform.mapRect(location);
                result.setLocation(location);
                mappedRecognitions.add(result);
            }
        }

        if (overlayView != null) {
            tracker.trackResults(mappedRecognitions, luminanceCopy, currTimestamp);
        }

        croppedBitmap.recycle();

        return mappedRecognitions;
    }


    public static List<Classifier.Recognition> recognizeObjects(Context context, int width, int height, int rotation, int screenOrientation, OverlayView overlayView,final Bitmap bitmap) {

        int cropSize;

        try {
            detector = TFLiteObjectDetectionAPIModel.create(
                    context.getAssets(),
                    TF_OD_API_MODEL_FILE,
                    TF_OD_API_LABELS_FILE,
                    TF_OD_API_INPUT_SIZE,
                    TF_OD_API_IS_QUANTIZED);
            cropSize = TF_OD_API_INPUT_SIZE;
        } catch (final IOException e) {
            Log.e("ExceptionInitClass", e.getMessage());
            Toast toast =
                    Toast.makeText(
                            context, "Classifier could not be initialized", Toast.LENGTH_SHORT);
            toast.show();
            return null;
        }

        previewWidth = width;
        previewHeight = height;

        sensorOrientation = rotation - screenOrientation;
        LOGGER.i("Camera orientation relative to screen canvas: %d", sensorOrientation);

        LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
        Bitmap croppedBitmap = Bitmap.createBitmap(cropSize, cropSize, Bitmap.Config.ARGB_8888);

        frameToCropTransform =
                ImageUtils.getTransformationMatrix(
                        previewWidth, previewHeight,
                        cropSize, cropSize,
                        sensorOrientation, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        frameToCropTransform.invert(cropToFrameTransform);

        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(bitmap, frameToCropTransform, null);

        final List<Classifier.Recognition> results = detector.recognizeImage(croppedBitmap);

        final List<Classifier.Recognition> mappedRecognitions = new LinkedList<>();

        for (final Classifier.Recognition result : results) {
            final RectF location = result.getLocation();
            if (location != null && result.getConfidence() >= MINIMUM_CONFIDENCE_TF_OD_API) {

                cropToFrameTransform.mapRect(location);
                result.setLocation(location);
                mappedRecognitions.add(result);
            }
        }

        croppedBitmap.recycle();

        return mappedRecognitions;
    }

}
