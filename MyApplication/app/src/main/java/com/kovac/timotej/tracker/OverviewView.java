package com.kovac.timotej.tracker;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;


public class OverviewView extends View {

    private final static String TAG = OverviewView.class.getSimpleName();

    public OverviewView(Context context) {
        super(context);
    }

    public OverviewView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public Tracker tracker;

    public void add(Tracker tracker) {
        this.tracker = tracker;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(tracker != null) {
            tracker.draw(canvas);
        }
    }

}
