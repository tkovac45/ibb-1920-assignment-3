package com.kovac.timotej.tracker;

import android.graphics.RectF;

public class BoundsChecker {

    // OLD : private static final double[] BORDER_LINES = new double[]{0.015, 0.031, 0.991, 0.935};
    private static final double[] BORDER_LINES = new double[]{0.025, 0.032, 0.985, 0.935};

    private static int width;
    private static int height;

    private static int previewWidth;
    private static int previewHeight;

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public static void setWidth(int w) {
        width = w;
    }

    public static void setHeight(int h) {
        height = h;
    }

    public static int getPreviewWidth() {
        return previewWidth;
    }

    public static int getPreviewHeight() {
        return previewHeight;
    }

    public static void setPreviewWidth(int previewWidth) {
        BoundsChecker.previewWidth = previewWidth;
    }

    public static void setPreviewHeight(int previewHeight) {
        BoundsChecker.previewHeight = previewHeight;
    }

    // Returns where the detection is touching the edge of the bitmap. Can be 0,1,2,3,4.
    public static int getTouchingSides(RectF location) {
        int touchingSides = 0;
        if(checkLeft(location.left)) {
            touchingSides++;
        }
        if(checkTop(location.top)) {
            touchingSides++;
        }
        if(checkRight(location.right)) {
            touchingSides++;
        }
        if(checkBottom(location.bottom)) {
            touchingSides++;
        }
        return touchingSides;
    }

    public static boolean isContained(RectF location) {
        boolean widthLimited = checkLeft(location.left) && checkRight(location.right);
        boolean heightLimited = checkTop(location.top) && checkBottom(location.bottom);

        return getTouchingSides(location) == 0 || widthLimited || heightLimited;

    }

    public static boolean checkLeft(float left) {
        return left < width * BORDER_LINES[0];
    }

    public static boolean checkTop(float top) {
        return top < height * BORDER_LINES[1];
    }

    public static boolean checkRight(float right) {
        return right > width * BORDER_LINES[2];
    }

    public static boolean checkBottom(float bottom) {
        return bottom > height * BORDER_LINES[3];
    }

}
