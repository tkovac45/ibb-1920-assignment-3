package com.kovac.timotej.tracker;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.demo.TensorFlowRecognizer;

import java.util.List;

public class CameraActivity extends AppCompatActivity implements SensorEventListener {

    private static final String TAG = CameraActivity.class.getSimpleName();

    public CustomCameraPreview mPreview;

    private Camera mCamera;
    private OverviewView overviewView;
    private SensorManager mSensorManager;
    private Sensor mCompass;
    private Sensor mRotationVector;
    private Bitmap bitmap;


    public int orientation = 0;

    private boolean tensorFlowInitialized = false;
    private boolean requestNewTensorFlowDetection = true;



    private final Handler handler = new Handler();


    private int frameCounter = 0;
    private int frameTimeSum = 0;

    private int cameraParametersWidth;
    private int cameraParametersHeight;

    static {
        System.loadLibrary("opencv_java3");
    }

    private long beginTime = 0;

    final Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {

        public void onPreviewFrame(byte[] data, Camera camera) {
            if (tensorFlowInitialized && requestNewTensorFlowDetection) {
                final long start = System.currentTimeMillis();

                if(beginTime == 0) {
                    beginTime = System.currentTimeMillis();
                }

                // Convert byte[] data to bitmap (using openCV for better performance)
                Mat mYuv = new Mat(cameraParametersHeight + cameraParametersHeight / 2, cameraParametersWidth, CvType.CV_8UC1);
                mYuv.put(0, 0, data);
                final Mat mRgba = new Mat();
                Imgproc.cvtColor(mYuv, mRgba, Imgproc.COLOR_YUV2BGR_NV12, 4);
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

                    bitmap = Bitmap.createBitmap(mRgba.cols(), mRgba.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(mRgba, bitmap);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(180);
                    Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
                    bitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

                } else {

                    if (orientation == 0)
                        Core.flip(mRgba.t(), mRgba, 1);
                    bitmap = Bitmap.createBitmap(mRgba.cols(), mRgba.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(mRgba, bitmap);
                    if (orientation == 3) {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(180);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
                        bitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                    }

                }


                // Run tensorflow and tracker
                requestNewTensorFlowDetection = false;
                Thread thread = new Thread() {
                    @Override
                    public void run() {

                        framesProcessed++;
                        overviewView.tracker.handleImage(bitmap);

                        requestNewTensorFlowDetection = true;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                overviewView.postInvalidate();
                            }
                        });

                        frameCounter++;
                        long time = (System.currentTimeMillis() - start);
                        frameTimeSum += time;
                        Log.d(TAG, "run: frame AVG TIME (" + frameCounter + "): " + (frameTimeSum / frameCounter) + ", CUR FRAME: " + time);


                        long elapsedMs = System.currentTimeMillis() - beginTime;
                        Log.d(TAG, "run: FPS = " + frameCounter / (elapsedMs / 1000f));


                    }
                };
                thread.setPriority(Thread.MAX_PRIORITY);
                thread.start();
            }
        }

    };

    private OrientationEventListener orientationListener;

    int MAXSAMPLES = 10;
    int tickindex=0;
    int ticksum=0;
    int ticklist[] = new int[MAXSAMPLES];

    private double CalcAverageTick(int newtick)
    {
        ticksum-=ticklist[tickindex];  /* subtract value falling off */
        ticksum+=newtick;              /* add new value */
        ticklist[tickindex]=newtick;   /* save new value so it can be subtracted later */
        if(++tickindex==MAXSAMPLES)    /* inc buffer index */
            tickindex=0;

        /* return average */
        return((double)ticksum/MAXSAMPLES);
    }





    int framesProcessed = 0;
    private FrameLayout frameCamera;

    public static boolean useTracker = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camera);


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.CAMERA}, 1000);
            return;
        }



        handleCreate();
    }


    private void handleCreate() {
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = Bitmap.createBitmap(10, 10, conf);

        TensorFlowRecognizer.recognizeObjects(CameraActivity.this, new Point(bitmap.getWidth(), bitmap.getHeight()), 0, 0, null, bitmap, getResources().getConfiguration().locale);

        Log.d(TAG, "InitTensorFlow task: tensorflow inited.");

        tensorFlowInitialized = true;



        Switch trackerSwitch = findViewById(R.id.tracker);
        trackerSwitch.setChecked(true);
        trackerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                overviewView.tracker.clearDetections();
                overviewView.tracker.handler.removeCallbacksAndMessages(null);

                useTracker = b;
            }
        });


        try {
            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            assert mSensorManager != null;
            mCompass = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        } catch (NullPointerException e) {
            Log.e(TAG, "onCreate: Error getting sensors - live detection speak canceled.");
            e.printStackTrace();
        }

        frameCamera = findViewById(R.id.frameCamera);

        overviewView = new OverviewView(this);
        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.flags =
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
        overviewView.setLayoutParams(localLayoutParams);

        Tracker tracker = new Tracker(this);
        overviewView.add(tracker);

        orientationListener = new OrientationEventListener(getApplicationContext(), SensorManager.SENSOR_DELAY_UI) {
            public void onOrientationChanged(int orientationChanged) {
                int newOrientation = orientation;
                if ((orientationChanged >= 0 && orientationChanged <= 40) || (orientationChanged >= 320 && orientationChanged <= 360)) {
                    newOrientation = 0;
                } else if (orientationChanged >= 230 && orientationChanged <= 310) {
                    newOrientation = 1;
                } else if (orientationChanged >= 140 && orientationChanged <= 210) {
                    //newOrientation = 2;
                } else if (orientationChanged >= 50 && orientationChanged <= 130) {
                    newOrientation = 3;
                }
                if (newOrientation != orientation) {
                    changeOrientation(newOrientation);
                }
            }
        };
    }



    private void cameraSetup() {
        mCamera = getCameraInstance();
        if (mCamera == null) {
            Log.d(TAG, "camera is not available!");
        } else {
            setCameraDisplayOrientation();

            Camera.Parameters params = mCamera.getParameters();
            if(params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)){
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            } else {
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }

            List<Camera.Size> sizes = params.getSupportedPictureSizes();
            params.setPictureSize(sizes.get(0).width, sizes.get(0).height);

            mCamera.setParameters(params);

            mPreview = new CustomCameraPreview(this, mCamera, previewCallback);

            Log.d(TAG, "cameraSetup: mPreview: width = " + mPreview.getWidth() + ", height = " + mPreview.getHeight());

            frameCamera.addView(mPreview);
            Log.d(TAG, "cameraSetup: frameCamera: width = " + frameCamera.getWidth() + ", height = " + frameCamera.getHeight());

            frameCamera.addView(overviewView,1);

        }

    }

    public OverviewView getOverviewView() {
        return overviewView;
    }

    private void changeOrientation(int newOrientation) {
        Log.d(TAG, "newOrientation: " + newOrientation);
        if (isCenteringObject) {
            return;
        }
        overviewView.tracker.clearDetections();
        orientation = newOrientation;
    }


    public void setupCameraParameters(int width, int height) {
        cameraParametersWidth = width;
        cameraParametersHeight = height;
    }


    private boolean isCenteringObject = false;


    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        }
        catch (Exception e){
            Log.e(TAG, e.getMessage());
        }
        return c;
    }

    public void setCameraDisplayOrientation() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(0, info);
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;
        } else {
            result = (info.orientation - degrees + 360) % 360;
        }
        mCamera.setDisplayOrientation(result);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        handleSensorsForTracker(event);
    }


    private void handleSensorsForTracker(SensorEvent event) {
        overviewView.tracker.handleSensors(event);
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    private boolean autoOrientation = false;

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called.");
        if (mCamera == null) {
            cameraSetup();
        }
        if (mSensorManager != null) {
            mSensorManager.registerListener(this, mCompass, SensorManager.SENSOR_DELAY_NORMAL);
            mSensorManager.registerListener(this, mRotationVector, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if(orientationListener != null) {
            if (orientationListener.canDetectOrientation() && autoOrientation) {
                orientationListener.enable();
            } else {
                orientationListener.disable();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mCamera != null) {
            mCamera.stopPreview();
            mPreview.getHolder().removeCallback(mPreview);
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
        if (mSensorManager != null)
            mSensorManager.unregisterListener(this);
        if (bitmap != null && !bitmap.isRecycled())
            bitmap.recycle();

        if(handler != null) {
            handler.removeCallbacks(null);
        }
        if(overviewView != null) {
            overviewView.tracker.handler.removeCallbacks(null);
            overviewView.tracker.clearDetections();
        }
        if(frameCamera != null)
            frameCamera.removeAllViews();
        if(orientationListener != null)
            orientationListener.disable();

        requestNewTensorFlowDetection = true;
        isCenteringObject = false;

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode == 1000) {
            //handleUtterance();
            if(permissions.length != grantResults.length) {
                Log.d(TAG, "onRequestPermissionsResult: lengths differ.");
                Toast.makeText(this, "You must grant requested permissions.", Toast.LENGTH_LONG).show();
                finish();
                return;
            }
            for(int grantResult : grantResults) {
                if(grantResult != PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "onRequestPermissionsResult: a permission has not been granted.");
                    Toast.makeText(this, "You must grant requested permissions.", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }
            }

            handleCreate();
        }
    }

}
