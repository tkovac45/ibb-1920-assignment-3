package com.kovac.timotej.tracker;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.os.Handler;
import android.text.TextPaint;
import android.util.Log;
import android.util.Pair;

import org.tensorflow.demo.Classifier;
import org.tensorflow.demo.TensorFlowRecognizer;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class Tracker {

    private static final String TAG = Tracker.class.getSimpleName();

    private static Paint rectPaint;
    private static Paint filledRectPaint;
    private static Paint highlightPaint;
    private static TextPaint textPaint;
    private static RectF screenRectF;

    // You have to get 5 point to add a new detection.
    private static final int ADD_THRESHOLD = 5;

    private static int[] COLORS = new int[]{
            0xFFFF0000,
            0xFF00FF00,
            0xFF0000FF,

            0xFF00FFFF,
            0xFFFF00FF,

            0xFFFF8800,
            0xFF88FF00,
            0xFF8800FF,

            0xFFFF0088,
            0xFF00FF88,
            0xFF0088FF,

            0xFFFF4400,
            0xFF44FF00,
            0xFF4400FF,

            0xFFFF0044,
            0xFF00FF44,
            0xFF0044FF,

            0xFFFFbb00,
            0xFFbbFF00,
            0xFFbb00FF,

            0xFFFF00bb,
            0xFF00FFbb,
            0xFF00bbFF,
    };

    public Handler handler;

    private WeakReference<CameraActivity> mActivityWeakReference;

    private List<Classifier.Recognition> recognizedObjects;
    private HashMap<String, Integer> pendingRecognitions;

    private int bitmapWidth;
    private int bitmapHeight;

    private boolean isScanning = false;

    private static final int TRACKER_MISS_BOUNDS = 175;
    private static final float TRACKER_MERGER_COVERAGE_SAME_TYPE = 0.65f;
    private static final float TRACKER_MERGER_COVERAGE_SAME_TYPE_SMALL = 0.04f;
    private static final float TRACKER_MERGER_ASPECT_SAME_TYPE = 0.10f;

    private boolean bTrackerInited = false;
    private int trackerIndex = 0;
    private TreeMap<Integer, Integer> trackerSpeakMap;

    private String highlightedObjectId = null;

    static {
        try {
            createPaints();
        } catch (Exception e) {
            Log.e(TAG, "Error creating paints.");
            e.printStackTrace();
        }
    }

    private static void createPaints() {
        rectPaint = new Paint();
        filledRectPaint = new Paint();
        textPaint = new TextPaint();
        highlightPaint = new Paint();

        rectPaint.setColor(Color.BLUE);
        rectPaint.setStyle(Paint.Style.STROKE);
        rectPaint.setStrokeWidth(8f);

        filledRectPaint.setColor(Color.BLUE);
        filledRectPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        filledRectPaint.setStrokeWidth(10f);

        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(32f);

        highlightPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    public Tracker(CameraActivity activity) {
        mActivityWeakReference = new WeakReference<>(activity);


        handler = new Handler();
        recognizedObjects = new ArrayList<>();
        pendingRecognitions = new HashMap<>();
        trackerSpeakMap = new TreeMap<>();

        bitmapWidth = 0;
        bitmapHeight = 0;
    }



    private Point trackerPoint;
    private List<Classifier.Recognition> newRecognizedObjects;
    private float[] prevVectorValues = null;

    public void handleImage(final Bitmap bmp) {
        if (bmp == null || bmp.isRecycled() || mActivityWeakReference.get() == null)
            return;

        bitmapWidth = bmp.getWidth();
        bitmapHeight = bmp.getHeight();

        BoundsChecker.setWidth(bmp.getWidth());
        BoundsChecker.setHeight(bmp.getHeight());
        screenRectF = new RectF(0, 0, BoundsChecker.getWidth(), BoundsChecker.getHeight());

        // Run tensorflow on thread with MAX priority - the heavier task (always runs longer than keypoints tracker)
        Thread recognizeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                newRecognizedObjects = TensorFlowRecognizer.recognizeObjects(mActivityWeakReference.get(), (byte[])null, new Point(bmp.getWidth(), bmp.getHeight()), 0,  0, null, bmp, mActivityWeakReference.get().getResources().getConfiguration().locale, 0.62f);
            }
        });

        recognizeThread.setPriority(Thread.MAX_PRIORITY);
        recognizeThread.start();

        // Run keypoints tracker on thread with MIN priority - the lighter task (always runs quicker than tensorflow)
        Thread trackerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (bTrackerInited) {
                    trackerPoint = FeatureDetection.trackerTrack(bmp, mActivityWeakReference.get().orientation);
                } else {
                    trackerPoint = FeatureDetection.trackerInit(bmp);
                    bTrackerInited = true;
                }
            }
        });

        trackerThread.setPriority(Thread.MIN_PRIORITY);
        trackerThread.start();

        // Wait for both threads to finish!
        try {
            recognizeThread.join();
            trackerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        float[] vectorValues = acquireVectorValues();

        float[] vectorValuesDiff = null;

        if (prevVectorValues != null) {
            Log.d(TAG, "vectorValues: (" + vectorValues[0] + ", " + vectorValues[1] + ", " + vectorValues[2] + "), prevVectorValues: (" +
                    prevVectorValues[0] + ", " + prevVectorValues[1] + ", " + prevVectorValues[2] + ")");

            vectorValuesDiff = new float[]{
                    vectorValues[0] - prevVectorValues[0],
                    vectorValues[1] - prevVectorValues[1],
                    vectorValues[2] - prevVectorValues[2]
            };
        }

        // Run tracker
        if(CameraActivity.useTracker) {
            track(newRecognizedObjects, trackerPoint, vectorValuesDiff);
        }
        else {
            recognizedObjects.clear();
            recognizedObjects.addAll(newRecognizedObjects);
        }

        prevVectorValues = vectorValues;
        bmp.recycle();
    }





    // Returns pair of strings, <tracked object id, tracked object title>
    @SuppressWarnings("unchecked")
    public Pair<String, String> getTrackerQueuedTitle() {
        String title = null;
        String id = null;

        TreeMap<Integer, Integer> trackerSpeakMapClone = (TreeMap<Integer, Integer>) trackerSpeakMap.clone();

        for (Map.Entry<Integer, Integer> entry : trackerSpeakMapClone.entrySet()) {
            Classifier.Recognition detection = getRecognitionWidthId(entry.getKey() + "");
            if (detection == null)
                continue;

            if (isIntersecting(screenRectF, detection.getLocation())) {
                if (entry.getValue() == 0) {
                    if (title == null) {
                        trackerSpeakMap.put(entry.getKey(), 1);
                        title = detection.getTitle();
                        id = detection.getId();
                    }
                }
            } else {

                if (entry.getValue() == 0 || entry.getValue() == 1) {
                    trackerSpeakMap.put(entry.getKey(), 2);
                }
            }
        }
        return new Pair<>(id, title);
    }

    public boolean isTrackedObjectStillOnScreen(String id) {
        Classifier.Recognition detection = getRecognitionWidthId(id);
        if (detection == null)
            return false;
        return isIntersecting(screenRectF, detection.getLocation());
    }

    private Classifier.Recognition getRecognitionWidthId(String id) {
        for (Classifier.Recognition detection : recognizedObjects) {
            if (detection.getId().equals(id)) {
                return detection;
            }
        }
        return null;
    }

    private long prevTime = -1;

    private void track(List<Classifier.Recognition> newRecognizedObjects, Point trackerDist, float[] vectorValuesDiff) {
        /*
               1. Apply vector values to distance
               2. Merge already recognized objects
               3. Move already recognized objects and remove off screen objects and objects with less than 0 confidence (EXCEPTION: highlighted object)
               4. Detect new recognized objects
               5. Handle pending recognitions
         */

        // 1. Apply vector values to distance
        if (vectorValuesDiff != null && mActivityWeakReference.get().orientation == 0 && mActivityWeakReference.get().getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
            Log.d(TAG, "vectorValuesDiff[0]: " + vectorValuesDiff[0] + ", vectorValuesDiff[1]: "  + vectorValuesDiff[1] + ", vectorValuesDiff[2]: " + vectorValuesDiff[2]);
            if (Math.abs(vectorValuesDiff[0]) > 1) {
                Log.d(TAG, "track, moving: " + trackerDist.toString() + ", vectorValuesDiff[0]: " + vectorValuesDiff[0] + ", moving trackerDist.y: " + (vectorValuesDiff[0] * 10));
                trackerDist.y += (vectorValuesDiff[0] * 10);
            } else if (Math.abs(vectorValuesDiff[2]) > 1) {
                Log.d(TAG, "track, moving: " + trackerDist.toString() + ", vectorValuesDiff[2]: " + vectorValuesDiff[2] + ", moving trackerDist.x: " + (vectorValuesDiff[2] * 10));
                trackerDist.x += (vectorValuesDiff[2] * 10);
            } else {
                Log.d(TAG, "track, moving: " + trackerDist.toString());
            }
        }

        // 2. Merge already recognized objects
        mergeRecognizedObjects();

        // 3. Move already recognized objects and remove off screen objects and objects with less than 0 confidence (EXCEPTION: highlighted object)
        int index = recognizedObjects.size() - 1;
        while (index >= 0) {
            RectF prevBounds = recognizedObjects.get(index).getLocation();
            recognizedObjects.get(index).setLocation(new RectF(
                    prevBounds.left + trackerDist.x,
                    prevBounds.top + trackerDist.y,
                    prevBounds.right + trackerDist.x,
                    prevBounds.bottom + trackerDist.y
            ));

            if (prevTime > 0) {
                recognizedObjects.get(index).setConfidence(recognizedObjects.get(index).getConfidence() - (0.0005f * (System.currentTimeMillis() - prevTime)));
            }

            if (BoundsChecker.checkLeft(prevBounds.right) || BoundsChecker.checkTop(prevBounds.bottom) ||
                    BoundsChecker.checkRight(prevBounds.left) || BoundsChecker.checkBottom(prevBounds.top)) {
                recognizedObjects.remove(index);
            } else if (recognizedObjects.get(index).getConfidence() < 0 && index != getIndexOfObjectById(highlightedObjectId) && !isScanning) {
                recognizedObjects.remove(index);
            }
            index--;
        }

        prevTime = System.currentTimeMillis();

        // 4. Detect new recognized objects
        for (Classifier.Recognition newRecognizedObject : newRecognizedObjects) {

            RectF loc = new RectF(
                    (newRecognizedObject.getLocation().top / BoundsChecker.getHeight()) * BoundsChecker.getWidth(),
                    (newRecognizedObject.getLocation().left / BoundsChecker.getWidth()) * BoundsChecker.getHeight(),
                    (newRecognizedObject.getLocation().bottom / BoundsChecker.getHeight()) * BoundsChecker.getWidth(),
                    (newRecognizedObject.getLocation().right / BoundsChecker.getWidth()) * BoundsChecker.getHeight()
            );

            if (mActivityWeakReference.get().orientation == 1) {
                float left = loc.left;
                loc.left = BoundsChecker.getWidth() - loc.right;
                loc.right = BoundsChecker.getWidth() - left;

                newRecognizedObject.setLocation(loc);
            } else if (mActivityWeakReference.get().orientation == 3) {
                float top = loc.top;
                loc.top = BoundsChecker.getHeight() - loc.bottom;
                loc.bottom = BoundsChecker.getHeight() - top;

                newRecognizedObject.setLocation(loc);
            }
            boolean isNewObject = true;
            for (int i = 0; i < recognizedObjects.size(); i++) {
                if (recognizedObjects.get(i).getTitle().contains(newRecognizedObject.getTitle()) && trackerCheckBounds(recognizedObjects.get(i).getLocation(), newRecognizedObject.getLocation())) {
                    isNewObject = false;
                    recognizedObjects.get(i).setLocation(newRecognizedObject.getLocation());
                    recognizedObjects.get(i).setConfidence(newRecognizedObject.getConfidence());
                    break;
                }
            }

            if (isNewObject) {
                // First add the object to pending recognitions with a score of 3.
                if(!pendingRecognitions.containsKey(newRecognizedObject.getTitle())) {
                    pendingRecognitions.put(newRecognizedObject.getTitle(), 3);
                }
                else {
                    // Increase the score of a pending recognition by 3.
                    int newScore = pendingRecognitions.get(newRecognizedObject.getTitle()) + 3;
                    if(newScore >= ADD_THRESHOLD) {
                        newRecognizedObject.setId(trackerIndex + "");

                        trackerIndex++;
                        recognizedObjects.add(newRecognizedObject);

                        trackerSpeakMap.put(Integer.parseInt(newRecognizedObject.getId()), 0);

                        pendingRecognitions.remove(newRecognizedObject.getTitle());

                        Log.d(TAG, "track: adding new detection = " + newRecognizedObject.getTitle());
                    }
                    else {
                        pendingRecognitions.put(newRecognizedObject.getTitle(), newScore);
                    }
                }
            }
        }

        // 5. Decrease scores of all pending recognitions by 1 and remove recognitions with score <= 0.
        Set<String> keysToRemove = new HashSet<>();
        for (Map.Entry<String, Integer> entry : pendingRecognitions.entrySet()) {
            int newValue = entry.getValue() - 1;
            if(newValue <= 0) {
                keysToRemove.add(entry.getKey());
                Log.d(TAG, "track: removing bad detection = " + entry.getKey());
            }
            pendingRecognitions.put(entry.getKey(), newValue);
        }
        pendingRecognitions.keySet().removeAll(keysToRemove);

    }

    private void mergeRecognizedObjects() {
        int i = 0;
        int j;
        while (i < recognizedObjects.size()) {
            j = 0;
            while (j < recognizedObjects.size()) {
                if (i != j) {
                    if (recognizedObjects.get(i).getTitle().equals(recognizedObjects.get(j).getTitle())) {
                        if (trackerCheckCoverage(recognizedObjects.get(i).getLocation(), recognizedObjects.get(j).getLocation())) {
                            int index = getIndexOfObjectById(highlightedObjectId);
                            if (i == index) {
                                recognizedObjects.get(i).setLocation(trackerUnionBounds(recognizedObjects.get(i).getLocation(), recognizedObjects.get(j).getLocation()));
                                recognizedObjects.remove(j);
                            } else if (j == index) {
                                recognizedObjects.get(j).setLocation(trackerUnionBounds(recognizedObjects.get(i).getLocation(), recognizedObjects.get(j).getLocation()));
                                recognizedObjects.remove(i);
                            } else if (j > i) {
                                recognizedObjects.get(i).setLocation(trackerUnionBounds(recognizedObjects.get(i).getLocation(), recognizedObjects.get(j).getLocation()));
                                recognizedObjects.remove(j);
                            } else {
                                recognizedObjects.get(j).setLocation(trackerUnionBounds(recognizedObjects.get(i).getLocation(), recognizedObjects.get(j).getLocation()));
                                recognizedObjects.remove(i);
                            }
                            break;
                        }
                    }
                }
                j++;
            }
            i++;
        }
    }

    private RectF trackerUnionBounds(RectF bounds1, RectF bounds2) {
        return new RectF(
                Math.min(bounds1.left, bounds2.left),
                Math.min(bounds1.top, bounds2.top),
                Math.max(bounds1.right, bounds2.right),
                Math.max(bounds1.bottom, bounds2.bottom)
        );
    }

    private boolean trackerCheckBounds(RectF bounds1, RectF bounds2) {
        return ((Math.abs(bounds1.centerX() - bounds2.centerX()) < TRACKER_MISS_BOUNDS && Math.abs(bounds1.centerY() - bounds2.centerY()) < TRACKER_MISS_BOUNDS) && bounds1.contains(bounds2)) ||
                trackerCheckCoverage(bounds1, bounds2);
    }

    private boolean trackerCheckCoverage(RectF bounds1, RectF bounds2) {
        RectF intersection = new RectF(
                Math.max(bounds1.left, bounds2.left),
                Math.max(bounds1.top, bounds2.top),
                Math.min(bounds1.right, bounds2.right),
                Math.min(bounds1.bottom, bounds2.bottom)
        );

        if (intersection.left >= intersection.right || intersection.top >= intersection.bottom)
            return false;

        float area1 = bounds1.width() * bounds1.height();
        float area2 = bounds2.width() * bounds2.height();
        float intersectArea = intersection.width() * intersection.height();

        if (intersectArea / area1 > TRACKER_MERGER_COVERAGE_SAME_TYPE || intersectArea / area2 > TRACKER_MERGER_COVERAGE_SAME_TYPE) {
            return true;
        }

        if (intersectArea / area1 > TRACKER_MERGER_COVERAGE_SAME_TYPE_SMALL || intersectArea / area2 > TRACKER_MERGER_COVERAGE_SAME_TYPE_SMALL) {

            if (Math.abs(
                    Math.min(bounds1.width(), bounds1.height()) / Math.max(bounds1.width(), bounds1.height()) -
                    Math.min(bounds2.width(), bounds2.height()) / Math.max(bounds2.width(), bounds2.height())) < TRACKER_MERGER_ASPECT_SAME_TYPE ) {

                if (Math.max(area1, area2) / Math.min(area1, area2) < 1.5) {

                    Log.d(TAG, "MERGED!");
                    return true;

                }

            }

        }
        return false;
    }

    private boolean isIntersecting(RectF bounds1, RectF bounds2) {
        RectF intersection = new RectF(
                Math.max(bounds1.left, bounds2.left),
                Math.max(bounds1.top, bounds2.top),
                Math.min(bounds1.right, bounds2.right),
                Math.min(bounds1.bottom, bounds2.bottom)
        );
        return !(intersection.left >= intersection.right) && !(intersection.top >= intersection.bottom);
    }

    public void draw(Canvas canvas) {
        Log.d(TAG, "draw: called.");
        if (recognizedObjects == null || recognizedObjects.size() < 1) {
            return;
        }

        BoundsChecker.setPreviewWidth(mActivityWeakReference.get().mPreview.getWidth());
        BoundsChecker.setPreviewHeight(mActivityWeakReference.get().mPreview.getHeight());

        RectF destBounds;

        int index = 0;

        int highlightedObjectIndex = getIndexOfObjectById(highlightedObjectId);

        for (Classifier.Recognition object : recognizedObjects) {

            destBounds = calculateBoundsWithAspect(object.getLocation(), bitmapWidth, bitmapHeight,
                    mActivityWeakReference.get().mPreview.getWidth(), mActivityWeakReference.get().mPreview.getHeight());

            rectPaint.setColor(COLORS[Integer.parseInt(object.getId()) % COLORS.length]);
            filledRectPaint.setColor(COLORS[Integer.parseInt(object.getId()) % COLORS.length]);

            if (highlightedObjectIndex == index) {
                rectPaint.setStrokeWidth(14f);

                highlightPaint.setColor(COLORS[Integer.parseInt(object.getId()) % COLORS.length] & 0x2AFFFFFF);
                canvas.drawRect(destBounds.left,
                        destBounds.top,
                        destBounds.right,
                        destBounds.bottom, highlightPaint);
            }

            canvas.drawRect(destBounds.left,
                    destBounds.top,
                    destBounds.right,
                    destBounds.bottom, rectPaint);

            if (highlightedObjectIndex == index) {
                rectPaint.setStrokeWidth(8f);
            }

            if (mActivityWeakReference.get().orientation == 0) {
                android.graphics.Rect destBounds1 = new android.graphics.Rect();
                textPaint.getTextBounds(object.getTitle().toUpperCase(), 0, object.getTitle().length(), destBounds1);

                canvas.drawRect(destBounds.left + destBounds1.left,
                        destBounds.top + destBounds1.top - 10,
                        destBounds.left + destBounds1.right,
                        destBounds.top + destBounds1.bottom - 10, filledRectPaint);

                canvas.drawText(object.getTitle().toUpperCase(),
                        destBounds.left,
                        destBounds.top - 10, textPaint);
            } else if (mActivityWeakReference.get().orientation == 1) {
                android.graphics.Rect destBounds1 = new android.graphics.Rect();
                textPaint.getTextBounds(object.getTitle().toUpperCase(), 0, object.getTitle().length(), destBounds1);






                //canvas.rotate(90, 0 + destBounds1.exactCenterX(),0 + destBounds1.exactCenterY());



                canvas.drawRect(destBounds.left + destBounds1.left,
                        destBounds.top + destBounds1.top - 10,
                        destBounds.left + destBounds1.right,
                        destBounds.top + destBounds1.bottom - 10, filledRectPaint);

                canvas.drawText(object.getTitle().toUpperCase(),
                        destBounds.left,
                        destBounds.top - 10, textPaint);

                //canvas.rotate(-90, 0 + destBounds1.exactCenterX(),0 + destBounds1.exactCenterY());
            } else if (mActivityWeakReference.get().orientation == 3) {
                android.graphics.Rect destBounds1 = new android.graphics.Rect();
                textPaint.getTextBounds(object.getTitle().toUpperCase(), 0, object.getTitle().length(), destBounds1);

                canvas.drawRect(destBounds.left + destBounds1.left,
                        destBounds.top + destBounds1.top - 10,
                        destBounds.left + destBounds1.right,
                        destBounds.top + destBounds1.bottom - 10, filledRectPaint);

                canvas.drawText(object.getTitle().toUpperCase(),
                        destBounds.left,
                        destBounds.top - 10, textPaint);
            }


            index++;
        }
    }

    // Takes location that is based on initWidth and initHeight and returns new location based on destWidth and destHeight
    public static RectF calculateBoundsWithAspect(RectF location, int initWidth, int initHeight, int destWidth, int destHeight) {
        RectF destBounds = new RectF(
                location.left,
                location.top,
                location.right,
                location.bottom
        );

        if (initWidth != destWidth) {
            float xx = destBounds.left / initWidth;
            destBounds.left = xx * destWidth;
            xx = destBounds.right / initWidth;
            destBounds.right = xx * destWidth;
        }

        if (initHeight != destHeight) {
            float yy = destBounds.top / initHeight;
            destBounds.top = yy * destHeight;
            yy = destBounds.bottom / initHeight;
            destBounds.bottom = yy * destHeight;
        }

        return destBounds;
    }



    private int getIndexOfObjectById(String id) {
        if (id == null)
            return -1;
        for (int i = 0; i < recognizedObjects.size(); i++) {
            if (recognizedObjects.get(i).getId().equals(id))
                return i;
        }
        return -1;
    }




    public String getHighlightedObjectTitle() {
        int index = getIndexOfObjectById(highlightedObjectId);
        if (index < 0)
            return null;
        return recognizedObjects.get(index).getTitle();
    }




    private float vectorX = 0, vectorY = 0, vectorZ = 0;

    public void handleSensors(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            vectorX = (float)Math.toDegrees(event.values[0]);
            vectorY = (float)Math.toDegrees(event.values[1]);
            vectorZ = (float)Math.toDegrees(event.values[2]);
        }
    }


    private float[] acquireVectorValues() {
        float[] vectorValues = new float[3];
        vectorValues[0] = vectorX;
        vectorValues[1] = vectorY;
        vectorValues[2] = vectorZ;
        return vectorValues;
    }

    public void clearDetections() {
        recognizedObjects = new ArrayList<>();
    }

}
