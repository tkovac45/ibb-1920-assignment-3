package com.kovac.timotej.tracker;


import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Point;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.lang.ref.WeakReference;
import java.util.List;


public class CustomCameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = CustomCameraPreview.class.getSimpleName();

    private WeakReference<CameraActivity> mActivityWeakReference;
    private final SurfaceHolder mHolder;
    private final Camera mCamera;
    private final List<Camera.Size> mSupportedPreviewSizes;
    private Camera.Size mPreviewSize;
    private Camera.PreviewCallback callback;

    public CustomCameraPreview(CameraActivity activity, Camera camera, Camera.PreviewCallback callback) {
        this(activity, camera);
        this.callback = callback;
    }

    public CustomCameraPreview(CameraActivity activity, Camera camera) {
        super(activity);
        mCamera = camera;

        this.mActivityWeakReference = new WeakReference<>(activity);

        mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();

        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        mCamera.setPreviewCallback(callback);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.d(TAG, "surfaceChanged: called");
        if (mHolder.getSurface() == null){
            return;
        }
        try {
            Log.d(TAG, "surfaceChanged: stopping preview.");
            mCamera.stopPreview();
        } catch (Exception e){
            Log.e("CustomCameraPreview", e.getMessage());
        }
        try {
            Log.d(TAG, "surfaceChanged: starting preview.");
            Camera.Parameters parameters = mCamera.getParameters();

            int desiredWidth = mPreviewSize.width / 2;
            int desiredHeight = mPreviewSize.height / 2;

            Log.d(TAG, "surfaceChanged: desiredWidth: " + desiredWidth + ", desiredHeight: " + desiredHeight);

            int bestIndex = 0;
            double bestScore = Double.MAX_VALUE;
            int index = 0;

            for (Camera.Size size : mSupportedPreviewSizes) {
                double score = (Math.abs(size.width - desiredWidth)) + (Math.abs(size.height - desiredHeight));

                double desiredRatio = (double)desiredWidth / desiredHeight;
                double ratio = (double)size.width / size.height;

                score += Math.abs(desiredRatio - ratio) * mPreviewSize.width;

                if (score < bestScore) {
                    bestScore = score;
                    bestIndex = index;
                    Log.d(TAG, "score, new best; width: " + mSupportedPreviewSizes.get(bestIndex).width + ", height: " + mSupportedPreviewSizes.get(bestIndex).height + ", score: " + score);
                }

                index++;
            }

            parameters.setPreviewSize(mSupportedPreviewSizes.get(bestIndex).width  ,mSupportedPreviewSizes.get(bestIndex).height );
            //parameters.setPreviewSize(1024, 768);

            mCamera.setParameters(parameters);

            Log.d(TAG, "orientation: " + mActivityWeakReference.get().getResources().getConfiguration().orientation);
            if (mActivityWeakReference.get().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                mCamera.setDisplayOrientation(180);
            } else {
                mCamera.setDisplayOrientation(90);
            }

            mCamera.setPreviewCallback(callback);
            mCamera.setPreviewDisplay(mHolder);

            mActivityWeakReference.get().setupCameraParameters(
                    mCamera.getParameters().getPreviewSize().width,
                    mCamera.getParameters().getPreviewSize().height);

            mCamera.startPreview();

        } catch (Exception e){
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        final int height = Resources.getSystem().getDisplayMetrics().heightPixels;

        Log.d(TAG, "onMeasure: widthPixels: " + width + ", heightPixels: " + height);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
        }

        Log.d(TAG, "onMeasure: optimalPreviewSize: width = " + mPreviewSize.width + ", height = " + mPreviewSize.height);

        try {

            WindowManager windowManager = (WindowManager) mActivityWeakReference.get().getApplication().getSystemService(Context.WINDOW_SERVICE);
            final Display display = windowManager.getDefaultDisplay();
            Point outPoint = new Point();
            display.getRealSize(outPoint);

            if (mPreviewSize != null) {
                if (mActivityWeakReference.get().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    float ratio;
                    if (mPreviewSize.height >= mPreviewSize.width)
                        ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
                    else
                        ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;
                    setMeasuredDimension(width, (int) (width * ratio));

                    float y = (outPoint.y - (int) (width * ratio));
                    y /= 2f;
                    setY(y);
                    mActivityWeakReference.get().getOverviewView().setY(y);

                } else {
                    float ratio;
                    if (mPreviewSize.width >= mPreviewSize.height)
                        ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;
                    else
                        ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
                    setMeasuredDimension((int) (height * ratio), height);

                    float x = (outPoint.x - (int) (height * ratio));
                    x /= 2f;
                    setX(x);
                    mActivityWeakReference.get().getOverviewView().setX(x);

                }
            }


        } catch (Exception e) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            Log.e(TAG, "exception: " + e.getMessage());
        }
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;

        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        for (Camera.Size size : sizes) {

            Log.d(TAG, "size, width: " + size.width + ", height: " + size.height);

            double ratio = (double) size.height / size.width;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            if (Math.abs(size.width - h) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.width - h);
            }

            Log.d(TAG, "New_size, width: " + size.width + ", height: " + size.height);
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - h) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - h);
                }
            }
        }

        return optimalSize;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {
            super.onDraw(canvas);
            Log.d(TAG, "onDraw: called.");
        } catch (Exception e) {
            Log.e(TAG, "Error: " + e.getMessage());
        }
    }

}
