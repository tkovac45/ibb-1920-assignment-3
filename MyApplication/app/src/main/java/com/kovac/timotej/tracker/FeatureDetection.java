package com.kovac.timotej.tracker;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.DMatch;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.util.LinkedList;
import java.util.List;

public class FeatureDetection {

    private static final String TAG = FeatureDetection.class.getSimpleName();

    private static FeatureDetector detector;
    private static DescriptorExtractor descriptor;
    private static DescriptorMatcher matcher;

    static {
        try {
            System.loadLibrary("opencv_java3");
            initializeOpenCVDependencies();
            Log.d(TAG, "OpenCV: Successfully loaded.");
        }
        catch (Exception e) {
            Log.e(TAG, "OpenCV: Unable to load OpenCV. Exception = " + e.getMessage());
            e.printStackTrace();
        }
    }

    FeatureDetection() { }


    private static void initializeOpenCVDependencies() {
        detector = FeatureDetector.create(FeatureDetector.ORB);
        descriptor = DescriptorExtractor.create(DescriptorExtractor.ORB);
        matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
    }


    private static Mat trackerDescriptors1, trackerDescriptors2;
    private static MatOfKeyPoint trackerKeypoints1, trackerKeypoints2;
    private static int trackerIndex = 0;

    public static Point trackerInit(Bitmap bitmap) {
        Mat img = new Mat();
        Utils.bitmapToMat(bitmap, img);
        Imgproc.cvtColor(img, img, Imgproc.COLOR_RGB2GRAY);

        trackerDescriptors1 = new Mat();
        trackerKeypoints1 = new MatOfKeyPoint();
        detector.detect(img, trackerKeypoints1);
        descriptor.compute(img, trackerKeypoints1, trackerDescriptors1);

        trackerIndex = 0;

        return new Point(0, 0);
    }


    public static Point trackerTrack(Bitmap bitmap, int orientation) {
        Point d;
        Mat img = new Mat();

        if (trackerIndex == 0) {
            Utils.bitmapToMat(bitmap, img);
            Imgproc.cvtColor(img, img, Imgproc.COLOR_RGB2GRAY);

            trackerDescriptors2 = new Mat();
            trackerKeypoints2 = new MatOfKeyPoint();
            detector.detect(img, trackerKeypoints2);
            descriptor.compute(img, trackerKeypoints2, trackerDescriptors2);

            d = trackerTrackSingle();
            d.x *= -1;
            d.y *= -1;

            trackerIndex = 1;
        } else {
            Utils.bitmapToMat(bitmap, img);
            Imgproc.cvtColor(img, img, Imgproc.COLOR_RGB2GRAY);

            trackerDescriptors1 = new Mat();
            trackerKeypoints1 = new MatOfKeyPoint();
            detector.detect(img, trackerKeypoints1);
            descriptor.compute(img, trackerKeypoints1, trackerDescriptors1);

            d = trackerTrackSingle();

            trackerIndex = 0;
        }

        if (orientation == 1) {
            int x = d.x;
            d.x = -1 * d.y;
            d.y = x;
        } else if (orientation == 3) {
            int x = d.x;
            d.x = d.y;
            d.y = -1 * x;
        }

        return d;
    }


    private static Point trackerTrackSingle() {
        MatOfDMatch matches = new MatOfDMatch();
        matcher.match(trackerDescriptors1, trackerDescriptors2, matches);

        List<DMatch> matchesList = matches.toList();

        Double min_dist = 150.0;

        for (int i = 0; i < matchesList.size(); i++) {
            Double dist = (double) matchesList.get(i).distance;
            if (dist < min_dist)
                min_dist = dist;
        }

        LinkedList<DMatch> good_matches = new LinkedList<>();
        for (int i = 0; i < matchesList.size(); i++) {
            if (matchesList.get(i).distance <= (3 * min_dist))
                good_matches.addLast(matchesList.get(i));
        }

        if (good_matches.size() < 1) {
            return new Point(0, 0);
        }

        KeyPoint[] keyPoints1 =  trackerKeypoints1.toArray();
        KeyPoint[] keyPoints2 =  trackerKeypoints2.toArray();

        Point point = new Point(0, 0);

        for (int i = 0; i < good_matches.size(); i++) {
            point.x += (int)(keyPoints1[good_matches.get(i).queryIdx].pt.x - keyPoints2[good_matches.get(i).trainIdx].pt.x);
            point.y += (int)(keyPoints1[good_matches.get(i).queryIdx].pt.y - keyPoints2[good_matches.get(i).trainIdx].pt.y);
        }

        point.x /= good_matches.size();
        point.y /= good_matches.size();

        return point;
    }

}
